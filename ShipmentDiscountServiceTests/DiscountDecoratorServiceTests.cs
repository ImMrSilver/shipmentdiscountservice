using Moq;
using ShipmentDiscountCalculator.Enums;
using ShipmentDiscountCalculator.Interface;
using ShipmentDiscountCalculator.Parsing;
using ShipmentDiscountCalculator.Service;
using System;
using Xunit;

namespace ShipmentDiscountServiceTests
{
    public class DiscountDecoratorServiceTests
    {
        private IDiscountService DiscountDecoratorService;
        private Mock<IShippingPriceRepository> ShippingPriceRepo = new Mock<IShippingPriceRepository>();

        public DiscountDecoratorServiceTests()
        {
            MockShipppingPriceRepo();
            DiscountDecoratorService = new DiscountDecoratorService(ShippingPriceRepo.Object);
        }

        [Theory]
        [InlineData("2015-02-01 S LP", "0")]
        [InlineData("2015-02-01 S MR", "0.50")]
        [InlineData("2015-02-03 L LP", "0")]
        [InlineData("2015-02-05 S LP", "0")]
        public void DiscountService_WhenProcessingSPackageSizeShipment_ReturnsDiscountToMatchSmallestPriceForSuchShipmentAmongAllProviders(string line, string discount)
        {
            //Arrange
            var transaction = Parser.GetTransaction(line);
            var expectedDiscount = Convert.ToDecimal(discount);
            //Act
            var discountAmount = DiscountDecoratorService.CalculateDiscount(transaction);
            //Assert
            Assert.Equal(expectedDiscount, discountAmount);
        }

        [Fact]
        public void DiscountService_WhenProcessingLPackageSizeLPProviderShipment_ReturnsFullDiscountForThirdShipmentOnACurrentMonth()
        {
            //Arrange
            var line1 = "2015-02-01 L LP";
            var line2 = "2015-02-01 S MR";
            var line3 = "2015-02-03 L LP";
            var line4 = "2015-02-05 L LP";

            var expectedDiscount1 = 0;
            var expectedDiscount2 = 0.50m;
            var expectedDiscount3 = 0;
            var expectedDiscount4 = 6.90m;

            var transaction1 = Parser.GetTransaction(line1);
            var transaction2 = Parser.GetTransaction(line2);
            var transaction3 = Parser.GetTransaction(line3);
            var transaction4 = Parser.GetTransaction(line4);
            //Act
            var discount1 = DiscountDecoratorService.CalculateDiscount(transaction1);
            var discount2 = DiscountDecoratorService.CalculateDiscount(transaction2);
            var discount3 = DiscountDecoratorService.CalculateDiscount(transaction3);
            var discount4 = DiscountDecoratorService.CalculateDiscount(transaction4);

            //Assert
            Assert.Equal(expectedDiscount1, discount1);
            Assert.Equal(expectedDiscount2, discount2);
            Assert.Equal(expectedDiscount3, discount3);
            Assert.Equal(expectedDiscount4, discount4);
        }

        [Fact]
        public void DiscountService_WhenProcessingThreeLPackageSizeLPProviderShipmentsInOneDay_ReturnsFullDiscountForThirdShipment()
        {
            //Arrange
            var line1 = "2015-02-01 L LP";
            var line2 = "2015-02-01 L LP";
            var line3 = "2015-02-01 L LP";
            var expectedDiscount1 = 0;
            var expectedDiscount2 = 0;
            var expectedDiscount3 = 6.90m;
            var transaction1 = Parser.GetTransaction(line1);
            var transaction2 = Parser.GetTransaction(line2);
            var transaction3 = Parser.GetTransaction(line3);

            //Act
            var discount1 = DiscountDecoratorService.CalculateDiscount(transaction1);
            var discount2 = DiscountDecoratorService.CalculateDiscount(transaction2);
            var discount3 = DiscountDecoratorService.CalculateDiscount(transaction3);

            //Assert
            Assert.Equal(expectedDiscount1, discount1);
            Assert.Equal(expectedDiscount2, discount2);
            Assert.Equal(expectedDiscount3, discount3);
        }

        [Fact]
        public void DiscountService_WhenProcessingThreeLPackageSizeShipmentsForTwoMonths_ReturnsDiscountsShipmentToLowestPriceAmongProviders()
        {
            //Arrange
            var line1 = "2015-02-01 L LP";
            var line2 = "2015-02-01 L LP";
            var line3 = "2015-02-01 L LP";
            var line4 = "2015-03-01 L LP";
            var line5 = "2015-03-01 L LP";
            var line6 = "2015-03-01 L LP";

            //2015-02
            var expectedDiscount1 = 0;
            var expectedDiscount2 = 0;
            var expectedDiscount3 = 6.90m;
            //2015-03
            var expectedDiscount4 = 0;
            var expectedDiscount5 = 0;
            var expectedDiscount6 = 6.90m;
        
            var transaction1 = Parser.GetTransaction(line1);
            var transaction2 = Parser.GetTransaction(line2);
            var transaction3 = Parser.GetTransaction(line3);
            var transaction4 = Parser.GetTransaction(line4);
            var transaction5 = Parser.GetTransaction(line5);
            var transaction6 = Parser.GetTransaction(line6);

            //Act
            var discount1 = DiscountDecoratorService.CalculateDiscount(transaction1);
            var discount2 = DiscountDecoratorService.CalculateDiscount(transaction2);
            var discount3 = DiscountDecoratorService.CalculateDiscount(transaction3);
            var discount4 = DiscountDecoratorService.CalculateDiscount(transaction4);
            var discount5 = DiscountDecoratorService.CalculateDiscount(transaction5);
            var discount6 = DiscountDecoratorService.CalculateDiscount(transaction6);

            //Assert
            Assert.Equal(expectedDiscount1, discount1);
            Assert.Equal(expectedDiscount2, discount2);
            Assert.Equal(expectedDiscount3, discount3);
            Assert.Equal(expectedDiscount4, discount4);
            Assert.Equal(expectedDiscount5, discount5);
            Assert.Equal(expectedDiscount6, discount6);
        }


        [Fact]
        public void DiscountService_WhenProcessingTransactions_AccumulatedDiscountsCantExceed10EuroInOneMonth()
        {
            //Arrange
            var line1 = "2015-02-01 L LP";
            var line2 = "2015-02-01 L LP";
            var line3 = "2015-02-01 L LP";
            var line4 = "2015-02-01 S MR";
            var line5 = "2015-02-01 S MR";
            var line6 = "2015-02-01 S MR";
            var line7 = "2015-02-01 S MR";
            var line8 = "2015-02-01 S MR";
            var line9 = "2015-02-01 S MR";
            var line10 = "2015-02-01 S MR";
            var line11 = "2015-02-01 S MR";

            var expectedDiscount1 = 0;
            var expectedDiscount2 = 0;
            var expectedDiscount3 = 6.90m;
            var expectedDiscount4 = 0.5m;
            var expectedDiscount5 = 0.5m;
            var expectedDiscount6 = 0.5m;
            var expectedDiscount7 = 0.5m;
            var expectedDiscount8 = 0.5m;
            var expectedDiscount9 = 0.5m;
            var expectedDiscount10 = 0.1m;
            var expectedDiscount11 = 0m;
            var expectedTotalDiscountSum = 10m;

            var transaction1 = Parser.GetTransaction(line1);
            var transaction2 = Parser.GetTransaction(line2);
            var transaction3 = Parser.GetTransaction(line3);
            var transaction4 = Parser.GetTransaction(line4);
            var transaction5 = Parser.GetTransaction(line5);
            var transaction6 = Parser.GetTransaction(line6);
            var transaction7 = Parser.GetTransaction(line7);
            var transaction8 = Parser.GetTransaction(line8);
            var transaction9 = Parser.GetTransaction(line9);
            var transaction10 = Parser.GetTransaction(line10);
            var transaction11 = Parser.GetTransaction(line11);

            //Act
            var discount1 = DiscountDecoratorService.CalculateDiscount(transaction1);
            var discount2 = DiscountDecoratorService.CalculateDiscount(transaction2);
            var discount3 = DiscountDecoratorService.CalculateDiscount(transaction3);
            var discount4 = DiscountDecoratorService.CalculateDiscount(transaction4);
            var discount5 = DiscountDecoratorService.CalculateDiscount(transaction5);
            var discount6 = DiscountDecoratorService.CalculateDiscount(transaction6);
            var discount7 = DiscountDecoratorService.CalculateDiscount(transaction7);
            var discount8 = DiscountDecoratorService.CalculateDiscount(transaction8);
            var discount9 = DiscountDecoratorService.CalculateDiscount(transaction9);
            var discount10 = DiscountDecoratorService.CalculateDiscount(transaction10);
            var discount11 = DiscountDecoratorService.CalculateDiscount(transaction11);

            var totalDiscountSum = discount1 + discount2 + discount3 + discount4 + discount5 +
                discount6 + discount7 + discount8 + discount9 + discount10 + discount11;
            //Assert
            Assert.True(expectedTotalDiscountSum >= totalDiscountSum);
            Assert.Equal(expectedDiscount1, discount1);
            Assert.Equal(expectedDiscount2, discount2);
            Assert.Equal(expectedDiscount3, discount3);
            Assert.Equal(expectedDiscount4, discount4);
            Assert.Equal(expectedDiscount5, discount5);
            Assert.Equal(expectedDiscount6, discount6);
            Assert.Equal(expectedDiscount7, discount7);
            Assert.Equal(expectedDiscount8, discount8);
            Assert.Equal(expectedDiscount9, discount9);
            Assert.Equal(expectedDiscount10, discount10);
            Assert.Equal(expectedDiscount11, discount11);
        }

        [Fact]
        public void DiscountService_WhenProcessingTransactionsForDifferentYears_DiscountsAreCalculatedCorrectly()
        {
            //Arrange
            var line1 = "2015-02-01 L LP";
            var line2 = "2015-02-01 L LP";
            var line3 = "2015-02-01 L LP";
            var line4 = "2015-03-01 L LP";
            var line5 = "2015-03-01 L LP";
            var line6 = "2015-03-01 L LP";
            var line7 = "2016-02-01 L LP";
            var line8 = "2016-02-01 S MR";
            var line9 = "2017-02-01 L LP";
            var line10 = "2018-02-01 L LP";
            var line11 = "2020-02-01 L LP";

            //2015-02
            var expectedDiscount1 = 0;
            var expectedDiscount2 = 0;
            var expectedDiscount3 = 6.90m;
            //2015-03
            var expectedDiscount4 = 0;
            var expectedDiscount5 = 0;
            var expectedDiscount6 = 6.90m;
            //2016
            var expectedDiscount7 = 0;
            var expectedDiscount8 = 0.5m;
            var expectedDiscount9 = 0;
            var expectedDiscount10 = 0m;
            var expectedDiscount11 = 0m;

            var transaction1 = Parser.GetTransaction(line1);
            var transaction2 = Parser.GetTransaction(line2);
            var transaction3 = Parser.GetTransaction(line3);
            var transaction4 = Parser.GetTransaction(line4);
            var transaction5 = Parser.GetTransaction(line5);
            var transaction6 = Parser.GetTransaction(line6);
            var transaction7 = Parser.GetTransaction(line7);
            var transaction8 = Parser.GetTransaction(line8);
            var transaction9 = Parser.GetTransaction(line9);
            var transaction10 = Parser.GetTransaction(line10);
            var transaction11 = Parser.GetTransaction(line11);

            //Act
            var discount1 = DiscountDecoratorService.CalculateDiscount(transaction1);
            var discount2 = DiscountDecoratorService.CalculateDiscount(transaction2);
            var discount3 = DiscountDecoratorService.CalculateDiscount(transaction3);
            var discount4 = DiscountDecoratorService.CalculateDiscount(transaction4);
            var discount5 = DiscountDecoratorService.CalculateDiscount(transaction5);
            var discount6 = DiscountDecoratorService.CalculateDiscount(transaction6);
            var discount7 = DiscountDecoratorService.CalculateDiscount(transaction7);
            var discount8 = DiscountDecoratorService.CalculateDiscount(transaction8);
            var discount9 = DiscountDecoratorService.CalculateDiscount(transaction9);
            var discount10 = DiscountDecoratorService.CalculateDiscount(transaction10);
            var discount11 = DiscountDecoratorService.CalculateDiscount(transaction11);

            //Assert
            Assert.Equal(expectedDiscount1, discount1);
            Assert.Equal(expectedDiscount2, discount2);
            Assert.Equal(expectedDiscount3, discount3);
            Assert.Equal(expectedDiscount4, discount4);
            Assert.Equal(expectedDiscount5, discount5);
            Assert.Equal(expectedDiscount6, discount6);
            Assert.Equal(expectedDiscount7, discount7);
            Assert.Equal(expectedDiscount8, discount8);
            Assert.Equal(expectedDiscount9, discount9);
            Assert.Equal(expectedDiscount10, discount10);
            Assert.Equal(expectedDiscount11, discount11);
        }

        private void MockShipppingPriceRepo()
        {
            ShippingPriceRepo.Setup(x => x.GetShippingPrice(ProviderEnum.LP, PackageSizeEnum.S)).Returns(1.5m);
            ShippingPriceRepo.Setup(x => x.GetShippingPrice(ProviderEnum.LP, PackageSizeEnum.M)).Returns(4.9m);
            ShippingPriceRepo.Setup(x => x.GetShippingPrice(ProviderEnum.LP, PackageSizeEnum.L)).Returns(6.9m);
            ShippingPriceRepo.Setup(x => x.GetShippingPrice(ProviderEnum.MR, PackageSizeEnum.S)).Returns(2m);
            ShippingPriceRepo.Setup(x => x.GetShippingPrice(ProviderEnum.MR, PackageSizeEnum.M)).Returns(3m);
            ShippingPriceRepo.Setup(x => x.GetShippingPrice(ProviderEnum.MR, PackageSizeEnum.L)).Returns(4m);
        }

    }
}
