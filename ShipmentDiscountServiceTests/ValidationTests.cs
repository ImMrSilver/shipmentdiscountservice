using ShipmentDiscountCalculator.Validation;
using System;
using Xunit;

namespace ShipmentDiscountServiceTests
{
    public class ValidationTests
    {
        [Theory]
        [InlineData("2015-02-01 SasdLP", "false")]
        [InlineData("2015-052-01 S MR", "false")]
        [InlineData("2015-02-03 LX LP", "false")]
        [InlineData("92015-02-05 S LP", "false")]
        [InlineData("92015-02-05 S L", "false")]
        [InlineData("2015-02-05 S LP", "true")]
        public void Validator_WhenValidatingIncorrectTransaction_ReturnsFalseForIncorrectTransactions(string line, string valid)
        {
            //Arrange
            var expectedIsValidValue = Convert.ToBoolean(valid);
            //Act
            var isValid = Validator.IsTransactionValid(line);
            //Assert
            Assert.Equal(expectedIsValidValue, isValid);
        }
    }
}
