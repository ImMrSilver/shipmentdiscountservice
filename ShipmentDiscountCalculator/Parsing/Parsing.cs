﻿using ShipmentDiscountCalculator.Enums;
using ShipmentDiscountCalculator.Model;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ShipmentDiscountCalculator.Parsing
{
    public static class Parser
    {
        public static Transaction GetTransaction(string line)
        {
            var lineWithSingleSpaces = Regex.Replace(line, @"\s+", " ");
            var transactionProperties = lineWithSingleSpaces.Split(" ");

            Enum.TryParse(transactionProperties[1], out PackageSizeEnum shipmentType);
            Enum.TryParse(transactionProperties[2], out ProviderEnum shippingcompany);
            var transactionTransaction = new Transaction()
            {
                Date = Convert.ToDateTime(transactionProperties[0]),
                Provider = shippingcompany,
                PackageSize = shipmentType
            };
            return transactionTransaction;
        }

        public static string GetInputFileDirectory()
        {
            var directory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            directory = directory + "/input/input.txt";
            return directory;
        }
    }
}
