﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShipmentDiscountCalculator.Enums
{
    public enum PackageSizeEnum
    {
        S,
        M,
        L
    }
}
