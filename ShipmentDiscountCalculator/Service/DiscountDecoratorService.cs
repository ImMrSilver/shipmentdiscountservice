﻿using ShipmentDiscountCalculator.Interface;
using ShipmentDiscountCalculator.Model;
using ShipmentDiscountCalculator.Model.DiscountDecorators;

namespace ShipmentDiscountCalculator.Service
{
    public class DiscountDecoratorService : IDiscountService
    {
        readonly private IShippingPriceRepository ShippingPriceRepository;
        private TransactionVO TransactionVO;

        public DiscountDecoratorService(
            IShippingPriceRepository ShippingPriceRepository
            )
        {
            this.ShippingPriceRepository = ShippingPriceRepository;
            TransactionVO = new TransactionVO
            {
                ShippingPriceRepository = this.ShippingPriceRepository
            };
        }

        public decimal CalculateDiscount(Transaction transaction)
        {
            TransactionVO.Transaction = transaction;
            var decorator = 
                new DiscountSumCantExceed10EuroInAMonth(
                new DiscountLPProviderThirdLPackageSizeShipmentInAMonth(
                new DiscountSPackageSizeShipmentsToLowestPriceAmongProviders(TransactionVO)));

            var discount = decorator.GetDiscount();
            TransactionVO.TotalDiscountForCurrentMonth += discount;
            return discount;
        }

        public string GetShippingPriceWithDiscount(Transaction transaction)
        {
            var discount = CalculateDiscount(transaction);
            var shippingPrice = ShippingPriceRepository.GetShippingPrice(transaction.Provider, transaction.PackageSize);
            return Formatting.Format.FormatDecimalValue(shippingPrice) + " " + Formatting.Format.FormatDecimalValue(discount);
        }
    }
}
