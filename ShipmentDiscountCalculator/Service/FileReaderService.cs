﻿using ShipmentDiscountCalculator.Interface;
using ShipmentDiscountCalculator.Parsing;
using System.Collections.Generic;
using System.IO;

namespace ShipmentDiscountCalculator.Service
{
    public class FileReaderService : IReader
    {
        public IEnumerable<string> ReadLines()
        {
            string directory = Parser.GetInputFileDirectory();
            string line;
            using (var reader = File.OpenText(directory))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        yield return line;
                    }
                }
            }
        }
}

