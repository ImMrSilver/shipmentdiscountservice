﻿using ShipmentDiscountCalculator.Formatting;
using ShipmentDiscountCalculator.Interface;
using ShipmentDiscountCalculator.Model;
using System;

namespace ShipmentDiscountCalculator.Service
{
    public class ConsoleWriterService : IWriter
    {
        public void WriteIgnoredTransaction(string transaction)
        {
            Console.WriteLine(transaction + " Ignored");
        }

        public void WriteProcessedTransaction(Transaction transaction, string shippingPriceWithDiscount)
        {
            Console.WriteLine(Format.FormantDate(transaction.Date) +" "+
                transaction.PackageSize.ToString() + " " +
                transaction.Provider.ToString() + " " +
                shippingPriceWithDiscount);
        }

        public void WriteError(string msg)
        {
            Console.WriteLine("ERROR: " +msg);
        }
    }
}
