﻿using ShipmentDiscountCalculator.Interface;
using ShipmentDiscountCalculator.Parsing;
using ShipmentDiscountCalculator.Validation;
using System;
using System.IO;

namespace ShipmentDiscountCalculator
{
    public class TransactionProcessingService: ITransactionProcessingService
    {
        readonly private IReader Reader;
        readonly private IWriter Writer;
        readonly private IDiscountService DiscountService;

        public TransactionProcessingService(
            IReader Reader,
            IWriter Writer,
            IDiscountService DiscountService)
        {
            this.Reader = Reader;
            this.Writer = Writer;
            this.DiscountService = DiscountService;
        }

        public void ProcessTransactions()
        {
            try
            {
                foreach (var line in Reader.ReadLines())
                {
                    if (Validator.IsTransactionValid(line))
                    {
                        var transaction = Parser.GetTransaction(line);
                        var shippingPriceWithDiscount = DiscountService.GetShippingPriceWithDiscount(transaction);
                        Writer.WriteProcessedTransaction(transaction, shippingPriceWithDiscount);
                    }
                    else
                    {
                        Writer.WriteIgnoredTransaction(line);
                    }
                }
            }
            catch (IOException e)
            {
                Writer.WriteError("IOException :" + e.Message);
            }
            catch (Exception ex)
            {
                Writer.WriteError("Internal application error :" + ex.Message);
            }
        }

    }
}
