﻿using ShipmentDiscountCalculator.Enums;

namespace ShipmentDiscountCalculator.Interface
{
    public interface IShippingPriceRepository
    {
        decimal GetShippingPrice(ProviderEnum Provider, PackageSizeEnum PackageSize);
    }
}
