﻿using ShipmentDiscountCalculator.Model;

namespace ShipmentDiscountCalculator.Interface
{
    public interface IWriter
    {
        void WriteProcessedTransaction(Transaction transaction, string shippingPriceWithDiscount);
        void WriteIgnoredTransaction(string transaction);
        void WriteError(string msg);
    }
}
