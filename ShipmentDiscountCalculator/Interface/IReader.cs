﻿using System.Collections.Generic;

namespace ShipmentDiscountCalculator.Interface
{
    public interface IReader
    {
        IEnumerable<string> ReadLines();
    }
}
