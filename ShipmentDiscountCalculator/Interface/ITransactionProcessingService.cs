﻿namespace ShipmentDiscountCalculator.Interface
{
    public interface ITransactionProcessingService
    {
        void ProcessTransactions();
    }
}
