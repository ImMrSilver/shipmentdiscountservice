﻿using ShipmentDiscountCalculator.Model;

namespace ShipmentDiscountCalculator.Interface
{
    public interface IDiscountService
    {
        decimal CalculateDiscount(Transaction transaction);
        string GetShippingPriceWithDiscount(Transaction transaction);
    }
}
