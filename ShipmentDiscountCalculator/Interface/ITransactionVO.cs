﻿using ShipmentDiscountCalculator.Model;

namespace ShipmentDiscountCalculator.Interface
{
    public interface ITransactionVO
    {
        TransactionVO GetTransactionVO();
        decimal GetDiscount();
    }
}
