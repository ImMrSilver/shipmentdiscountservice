﻿using ShipmentDiscountCalculator.Interface;
using ShipmentDiscountCalculator.Repository;
using ShipmentDiscountCalculator.Service;
using System;

namespace ShipmentDiscountCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            IReader Reader = new FileReaderService();
            IWriter Writer = new ConsoleWriterService();
            IShippingPriceRepository ShippingPriceRepository = new ShippingPriceRepository();
            IDiscountService DiscountDecoratorService = new DiscountDecoratorService(ShippingPriceRepository);
            ITransactionProcessingService TransactionProcessingService =
                new TransactionProcessingService
                (
                    Reader,
                    Writer,
                    DiscountDecoratorService
                );
            TransactionProcessingService.ProcessTransactions();
            Console.ReadLine();
        }
    }
}
