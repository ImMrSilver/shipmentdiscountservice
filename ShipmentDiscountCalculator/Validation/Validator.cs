﻿using ShipmentDiscountCalculator.Enums;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ShipmentDiscountCalculator.Validation
{
    public static class Validator
    {
        public static bool IsTransactionValid(string line)
        {
            var lineWithSingleSpaces = Regex.Replace(line, @"\s+", " ");
            var transactionProperties = lineWithSingleSpaces.Split(" ");

            if (transactionProperties.Length != 3)
            {
                return false;
            }

            try
            {
                var Date = Convert.ToDateTime(transactionProperties[0]);
                bool PackageSizeEnumNotFound = !Enum.GetNames(typeof(PackageSizeEnum)).Contains(transactionProperties[1]);
                bool ProviderEnumNotFound = !Enum.GetNames(typeof(ProviderEnum)).Contains(transactionProperties[2]);
                if (PackageSizeEnumNotFound || ProviderEnumNotFound)
                {
                    return false;
                };
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
