﻿using ShipmentDiscountCalculator.Enums;
using ShipmentDiscountCalculator.Interface;
using System.Collections.Generic;

namespace ShipmentDiscountCalculator.Repository
{
    public class ShippingPriceRepository : IShippingPriceRepository
    {
        private readonly Dictionary<ProviderEnum, Dictionary<PackageSizeEnum, decimal>> ShippingPrice = new Dictionary<ProviderEnum, Dictionary<PackageSizeEnum, decimal>>();

        public ShippingPriceRepository()
        {
            SetProviderPricing();
        }

        public decimal GetShippingPrice(ProviderEnum Provider, PackageSizeEnum PackageSize)
        {
            return ShippingPrice[Provider][PackageSize];
        }

        private void SetProviderPricing()
        {
            var lpShipmentPricingDictionary = new Dictionary<PackageSizeEnum, decimal>();
            lpShipmentPricingDictionary.Add(PackageSizeEnum.S, 1.5m);
            lpShipmentPricingDictionary.Add(PackageSizeEnum.M, 4.9m);
            lpShipmentPricingDictionary.Add(PackageSizeEnum.L, 6.9m);

            var mrShipmentPricingDictionary = new Dictionary<PackageSizeEnum, decimal>();
            mrShipmentPricingDictionary.Add(PackageSizeEnum.S, 2m);
            mrShipmentPricingDictionary.Add(PackageSizeEnum.M, 3m);
            mrShipmentPricingDictionary.Add(PackageSizeEnum.L, 4m);

            ShippingPrice.Add(ProviderEnum.LP, lpShipmentPricingDictionary);
            ShippingPrice.Add(ProviderEnum.MR, mrShipmentPricingDictionary);
        }
    }
}
