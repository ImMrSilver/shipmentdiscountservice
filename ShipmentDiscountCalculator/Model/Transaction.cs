﻿using ShipmentDiscountCalculator.Enums;
using System;

namespace ShipmentDiscountCalculator.Model
{
    public class Transaction
    {
        public DateTime Date { get; set; }
        public ProviderEnum Provider { get; set; }
        public PackageSizeEnum PackageSize { get; set; }
    }
}
