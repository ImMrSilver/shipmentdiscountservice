﻿using ShipmentDiscountCalculator.Interface;
using System;

namespace ShipmentDiscountCalculator.Model
{
    public class TransactionVO: ITransactionVO
    {
        public DateTime? FirstMonthlyTransactionDate { get; set; }
        
        public int LPProviderLPackageSizeTransactionCountForCurrentMonth { get; set; }

        public decimal TotalDiscountForCurrentMonth { get; set; }

        public Transaction Transaction { get; set; }

        public IShippingPriceRepository ShippingPriceRepository { get; set; }

        public decimal GetDiscount()
        {
            return 0.0m;
        }

        public TransactionVO GetTransactionVO()
        {
            return this;
        }
    }
}
