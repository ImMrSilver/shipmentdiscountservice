﻿using ShipmentDiscountCalculator.Enums;
using ShipmentDiscountCalculator.Interface;
using System;

namespace ShipmentDiscountCalculator.Model.DiscountDecorators
{
    public class DiscountLPProviderThirdLPackageSizeShipmentInAMonth : DiscountTransactionVO
    {
        public DiscountLPProviderThirdLPackageSizeShipmentInAMonth(ITransactionVO TransactionVO)
            : base(TransactionVO)
        {
            var transactionVO = GetTransactionVO();
            if (transactionVO.Transaction.PackageSize == PackageSizeEnum.L && transactionVO.Transaction.Provider == ProviderEnum.LP)
            {
                SetTransactionDateAndTransactionCount(transactionVO);
                SetDiscount(transactionVO);
            }
        }

        private static void SetTransactionDateAndTransactionCount(TransactionVO transactionVO)
        {
            if (transactionVO.FirstMonthlyTransactionDate == null)
            {
                transactionVO.FirstMonthlyTransactionDate = transactionVO.Transaction.Date;
                transactionVO.LPProviderLPackageSizeTransactionCountForCurrentMonth = 1;
            }
            else
            {
                if (transactionVO.Transaction.Date > transactionVO.FirstMonthlyTransactionDate &&
                        transactionVO.Transaction.Date.Month > ((DateTime)transactionVO.FirstMonthlyTransactionDate).Month)
                {
                    transactionVO.FirstMonthlyTransactionDate = transactionVO.Transaction.Date;
                    transactionVO.LPProviderLPackageSizeTransactionCountForCurrentMonth = 1;
                    transactionVO.TotalDiscountForCurrentMonth = 0;
                }
                else
                {
                    transactionVO.LPProviderLPackageSizeTransactionCountForCurrentMonth++;
                }
            }
        }

        private void SetDiscount(TransactionVO transactionVO)
        {
            if (transactionVO.LPProviderLPackageSizeTransactionCountForCurrentMonth == 3)
            {
                Discount = transactionVO.ShippingPriceRepository.GetShippingPrice(transactionVO.Transaction.Provider, transactionVO.Transaction.PackageSize);
            }
        }
    }
}
