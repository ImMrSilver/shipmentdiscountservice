﻿using ShipmentDiscountCalculator.Interface;

namespace ShipmentDiscountCalculator.Model.DiscountDecorators
{
    public class DiscountSumCantExceed10EuroInAMonth : DiscountTransactionVO
    {
        public DiscountSumCantExceed10EuroInAMonth(ITransactionVO TransactionVO)
            : base(TransactionVO)
        {
            var transactionVO = GetTransactionVO();
            var discount = GetDiscount();
            if (transactionVO.TotalDiscountForCurrentMonth + discount >= 10)
            {
                var difference = 10 - transactionVO.TotalDiscountForCurrentMonth;
                Discount = difference-discount;
            }
        }
    }
}
