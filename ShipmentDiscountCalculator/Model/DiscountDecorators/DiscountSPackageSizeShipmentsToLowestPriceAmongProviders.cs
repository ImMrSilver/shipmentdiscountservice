﻿using ShipmentDiscountCalculator.Enums;
using ShipmentDiscountCalculator.Interface;
using System;

namespace ShipmentDiscountCalculator.Model.DiscountDecorators
{
    public class DiscountSPackageSizeShipmentsToLowestPriceAmongProviders : DiscountTransactionVO
    {
        public DiscountSPackageSizeShipmentsToLowestPriceAmongProviders(ITransactionVO TransactionVO)
            : base(TransactionVO)
        {
            var transactionVO = GetTransactionVO();
            if (transactionVO.Transaction.PackageSize == PackageSizeEnum.S)
            {
                GetLowestSPackageSizeShipmentPrice(transactionVO);
            }
        }

        private void GetLowestSPackageSizeShipmentPrice(TransactionVO transactionVO)
        {
            var currentProviderSpackagePrice = transactionVO.ShippingPriceRepository.GetShippingPrice(transactionVO.Transaction.Provider, PackageSizeEnum.S);
            var lowestSPackagePrice = 0.0m;
            var values = Enum.GetValues(typeof(ProviderEnum));
            foreach (ProviderEnum provider in values)
            {
                lowestSPackagePrice = SetLowestPackagePrice(transactionVO, lowestSPackagePrice, provider);
            }
            var discount = 0.0m;
            if (lowestSPackagePrice < currentProviderSpackagePrice)
            {
                discount = currentProviderSpackagePrice - lowestSPackagePrice;
            }
            Discount = discount;
        }

        private static decimal SetLowestPackagePrice(TransactionVO transactionVO, decimal lowestSPackagePrice, ProviderEnum provider)
        {
            if (lowestSPackagePrice == 0)
            {
                lowestSPackagePrice = transactionVO.ShippingPriceRepository.GetShippingPrice(provider, PackageSizeEnum.S);
            }
            if (lowestSPackagePrice > transactionVO.ShippingPriceRepository.GetShippingPrice(provider, PackageSizeEnum.S))
            {
                lowestSPackagePrice = transactionVO.ShippingPriceRepository.GetShippingPrice(provider, PackageSizeEnum.S);
            }

            return lowestSPackagePrice;
        }
    }
}
