﻿using ShipmentDiscountCalculator.Interface;

namespace ShipmentDiscountCalculator.Model.DiscountDecorators
{
    public abstract class DiscountTransactionVO : ITransactionVO
    {
        ITransactionVO TransactionVO;

        protected decimal Discount = 0.0m;

        public DiscountTransactionVO(ITransactionVO TransactionVO)
        {
            this.TransactionVO = TransactionVO;
        }

        public decimal GetDiscount()
        {
            return TransactionVO.GetDiscount() + Discount;
        }

        public TransactionVO GetTransactionVO()
        {
            return TransactionVO.GetTransactionVO();
        }
    }
}
