﻿using System;

namespace ShipmentDiscountCalculator.Formatting
{
    public static class Format
    {
        public static string FormantDate(DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        public static string FormatDecimalValue(decimal value)
        {
            if (value == 0)
            {
                return "-";
            }
            else
            {
                var result = Math.Round(value, 2);
                return string.Format("{0:0.00}", result);
            }
        }
    }
}
